
<div id="TOC">
<ul>
<li><a href="#introduction">Introduction</a><ul>
<li><a href="#preliminary-results-and-notation">Preliminary results and notation</a></li>
<li><a href="#ellipsoids">Ellipsoids</a><ul>
<li><a href="#affine-transformations">Affine transformations</a></li>
<li><a href="#eigenvalues">Eigenvalues</a></li>
<li><a href="#minimum-volume-bounding-ellipsoid">Minimum volume bounding ellipsoid</a></li>
<li><a href="#properties">Properties</a></li>
<li><a href="#computational-comments">Computational comments</a></li>
</ul></li>
</ul></li>
<li><a href="#case-study-evaluating-the-shape-of-cities-and-neighbourhoods">Case study: evaluating the shape of cities and neighbourhoods</a><ul>
<li><a href="#city-wide-comparisons">City-wide comparisons</a></li>
<li><a href="#neighbourhood-level-comparisons">Neighbourhood-level comparisons</a></li>
</ul></li>
</ul>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML"> -->
<!-- </script> -->
<div id="introduction" class="section level1">
<h1>Introduction</h1>
<p>In a preceding post, I explored how graph theory could be used to evaluate the neighbourhood structure of cities. I namely illustrated that:</p>
<ul>
<li>The graph diameter and radius can be exploited to determine the peripheral and central neigbourhoods,</li>
<li>Cliques can be used to identify natural neighbourhood clusters,</li>
<li>Node degrees and peculiar subgraphs like stars can help identify more densely populated pockets located within large sparsely populated regions.</li>
</ul>
<p>I also suggested that this methodology could be used to evaluate the overall shape of cities. For instance, although Montreal is much denser than Quebec City, qualitative properties of its graph diameter seemed to suggest it was much more elongated than the later.</p>
<p>This post builds on this intuition. It provides formal tools to evaluate the shape of regions delimited by polygons or sets of points which are commonly used by GIS software. More precisely, by solving a convex optimization problem, we may find a minimum volume bounding ellipsoid that contains the region studied. We then take the ratio of the minimum and maximum eigenvalues of the positive definite matrix defining this ellipsoid. This provides a simple and tractable way to determine if a region is ellongated or round, which may be useful to evaluate the “compactness”, perhaps for gerrymandering purposes. Importantly, the method is invariant to scaling and shifting.</p>
<div id="preliminary-results-and-notation" class="section level2">
<h2>Preliminary results and notation</h2>
<p>We define the set of real symmetric positive definite <span class="math inline">\(n\)</span> dimensional matrices as <span class="math inline">\(\mathbb{S}_{++}^n\)</span>, that is the set of matrices <span class="math inline">\(X\in \mathbb{R}^{n \times n}: u^{\top}Xu &gt;0 , \forall u \neq 0\)</span> and <span class="math inline">\(X=X^{\top}\)</span>. We can assume without loss of generality that <span class="math inline">\(X\)</span> is full rank, since we could remove colinear rows and columns and we are only interested in non-degenerate ellipsoids.</p>
<p>For some matrix <span class="math inline">\(X \in \mathbb{S}_{++}^k\)</span>, we can write <span class="math inline">\(X=LL^{\top}\)</span> for <span class="math inline">\(L \in \mathbb{R}^{n \times n}\)</span> a lower diagonal matrix and the <a href="https://en.wikipedia.org/wiki/Cholesky_decomposition">Choleksi decomposition</a> is unique. Since <span class="math inline">\(det(X) = det(L)^2\)</span>, we note that <span class="math inline">\(det(X) &gt; 0\)</span> and <span class="math inline">\(det(L)&gt;0\)</span> if <span class="math inline">\(X\)</span> is full rank. Using the spectral decomposition <span class="math inline">\(X= U\Lambda U^{\top}\)</span> for some diagonal <span class="math inline">\(\Lambda\)</span> and unit matrix <span class="math inline">\(U\)</span>, it also follows that in this case all eigenvalues must be positive since <span class="math inline">\(det(X) = det(\Lambda) = \Pi_{i=1}^n \lambda_i\)</span>.</p>
<p>For any <span class="math inline">\(X \in \mathbb{S}_{++}^k\)</span>, we can also show that there exists a unique <span class="math inline">\(A \in \mathbb{S}_{++}^n\)</span> such that <span class="math inline">\(X=A^2\)</span>. Existence is easy to check by setting <span class="math inline">\(A=U\Lambda^{1/2} U^{\top}\)</span> and is sufficient for our purposes. For more details, see these <a href="http://www.math.drexel.edu/~foucart/TeachingFiles/F12/M504Lect7.pdf">lecture notes</a>.</p>
</div>
<div id="ellipsoids" class="section level2">
<h2>Ellipsoids</h2>
<p>An ellipsoid is a convex set that can be expressed as:</p>
<p><span class="math display">\[\mathcal{E} = \{u \in \mathbb{R}^n : (u-c)^{\top} X (u-c) \leq 1 \}\]</span></p>
<p>where <span class="math inline">\(X \in \mathbb{S}_{++}^n\)</span> is a positive definite matrix and <span class="math inline">\(c\in \mathbb{R}^n\)</span> is its center (see the classic <a href="https://web.stanford.edu/~boyd/cvxbook/bv_cvxbook.pdf">Boyd and Vanderberghe book</a>).</p>
<div id="affine-transformations" class="section level3">
<h3>Affine transformations</h3>
<p>An ellipsoid is an affine transformation of the unit ball. Using the Choleski decomposition, we have:</p>
<p><span class="math display">\[
 \begin{align}
 \mathcal{E} &amp;= \{u: (u-c)^{\top} X (u-c) \leq 1  \} \\
             &amp;= \{u: (L^{\top}(u-c))^{\top} (L^{\top}(u-c)) \leq 1  \}  \\
             &amp; = \{L^{-\top}a + c: ||a||_2 \leq 1  \}  \\
             &amp; = L^{-\top} \mathcal{B}_n+ c
 \end{align}
 \]</span></p>
<p>where <span class="math inline">\(\mathcal{B}_n\)</span> is the unit ball in <span class="math inline">\(\mathbb{R}^n\)</span> and <span class="math inline">\(L^{\top}\)</span> is lower triangular.</p>
<p>Using the fact that a real symetric matrix can be diagonalized using the spectral decomposition, we can also express <span class="math inline">\(\mathcal{E}\)</span> as:</p>
<p><span class="math display">\[ 
 \begin{align}
 \mathcal{E} &amp;= \{u: (u-c))^{\top} U \Lambda U^{\top} (u-c) \leq 1  \} \\
             &amp; = \{u: (\Lambda^{1/2} U^{\top} (u-c))^{\top} (\Lambda^{1/2} U^{\top}(u-c)) \leq 1  \} \\
             &amp; = U^{-\top} \Lambda^{-1/2}  \mathcal{B}_n+ c
 \end{align}
 \]</span> However, the most useful form uses the fact that every symmetric positive definite matrix can be expressed as a symmetric positive definite square root:</p>
<p><span class="math display">\[ 
 \begin{align}
 \mathcal{E} &amp;= \{u: (u-c))^{\top} AA (u-c) \leq 1  \} \\
             &amp; = \{u: (A (u-c))^{\top} (A (u-c)) \leq 1  \} \\
             &amp; = A^{-1}   \mathcal{B}_n+ c
 \end{align}
 \]</span> for some <span class="math inline">\(A \in \mathbb{S}_{++}^n\)</span> such that <span class="math inline">\(A^2=X\)</span>.</p>
</div>
<div id="eigenvalues" class="section level3">
<h3>Eigenvalues</h3>
<p>By exploiting the fact that the ellipsoid <span class="math inline">\(\mathcal{E}\)</span> is defined by the affine transformation <span class="math inline">\(x\rightarrow A^{-1} x +c\)</span>, we can determine its ellongation by comparing the ratio of the mininum and maximum eigenvalues of <span class="math inline">\(L\)</span> or <span class="math inline">\(L^{\top}\)</span>, which are equal or equivalently the square root of the eigenvalues of <span class="math inline">\(X\)</span>. Indeed, rotating the points <span class="math inline">\(s_i \in S\)</span> using <span class="math inline">\(U\)</span> expresses them using the orthogonal basis formed by the eigenvectors of <span class="math inline">\(X\)</span>, which are the same as those of <span class="math inline">\(L\)</span> and <span class="math inline">\(L^{\top}\)</span>. Comparing the ratio of eigenvalues therefore allows us to determine the relative importance of each axis. In 2 dimensions we would compare the principal and minor axis, which correspond to the width and height of the ellipsoid.</p>
<p><a href="%22/images/2019/11/ellipsoid/ellipsoidRotation.png%22">Comparing the shape of Montreal (left) and Quebec City (right)</a></p>
</div>
<div id="minimum-volume-bounding-ellipsoid" class="section level3">
<h3>Minimum volume bounding ellipsoid</h3>
<p>We can also show that the volume of the ellipsoid is equal to:</p>
<p><span class="math display">\[det(L^{-\top}) Vol(\mathcal{B}_n) = det(A^{-1}) Vol(\mathcal{B}_n) = det(X^{-1})^{\frac{1}{2}} Vol(\mathcal{B}_n) = det(U^{-\top}) \Pi_{i=1}^n \lambda_i^{-1/2} Vol(\mathcal{B}_n) = \Pi_{i=1}^n \lambda_i^{-1/2} Vol(\mathcal{B}_n)  \]</span> by noting that the determinant of the unitary matrix <span class="math inline">\(U\)</span> is 1 (see these <a href="http://www.sfu.ca/~mdevos/notes/semidef/ellipsoid.pdf">lecture notes</a> for further details). We observe that the eigenvalues of <span class="math inline">\(L\)</span> and <span class="math inline">\(A\)</span> are the square root of those of <span class="math inline">\(X\)</span>.</p>
<p>It follows that we can find the minimum volume ellipsoid that contains all points in the set <span class="math inline">\(S\)</span> by solving following convex optimization problem:</p>
<p><span class="math display">\[A^{*},c^* \in \displaystyle \arg\min_{A \in \mathbb{S}_{++}^n, c} \{ -\log(\det(A)) : ||A(s-c)||_2\leq 1 , \forall s \in S \}\]</span></p>
<p>This problem is convex since it consists of minimizing <span class="math inline">\(-log (det)\)</span>, which is convex over <span class="math inline">\(S_{++}^n\)</span> (see page 74 of <a href="https://web.stanford.edu/~boyd/cvxbook/bv_cvxbook.pdf">Boyd and Vanderberghe book</a>) while respecting convex second order cone constraints.</p>
<p>For a general set <span class="math inline">\(S\)</span>, this would result in a hard semi-infinite optimization problem. Indeed, there are as many second-order cone constraints as the cardinality of <span class="math inline">\(S\)</span> and <span class="math inline">\(\max_{s \in S} ||A(s-c)||_2\)</span> amounts to <em>maximizing</em> a convex quadratic program over a possibly non-convex set <span class="math inline">\(S\)</span>, which is intractable.</p>
<p>However, if <span class="math inline">\(S\)</span> is a finite discrete set <span class="math inline">\(S=\{s_1, \cdots, s_N \}\)</span>, we obtain a finite number of convex inequalities and we can solve the problem fairly efficiently using interior point solvers.</p>
</div>
<div id="properties" class="section level3">
<h3>Properties</h3>
<p>Consider the following restriction of the initial problem:</p>
<p><span class="math display">\[A^{*},c^* \in \displaystyle \arg\min_{A \in \mathbb{S}_{++}^n, c} \{ -\log(\det(A)) : ||A(s-c)||_2\leq 1 , \forall s \in conv(S) \}\]</span></p>
<p>where <span class="math inline">\(conv(S)\)</span> represents the convex hull of <span class="math inline">\(S\)</span>. If we denote <span class="math inline">\(\mathcal{E}^{S}\)</span> and <span class="math inline">\(\mathcal{E}^{conv(S)}\)</span> the ellipsoid obtained from <span class="math inline">\((A^* , c^*)\)</span> and <span class="math inline">\((A^{conv(S) , c^{conv(S) )\)</span>, the optimal solutions of the original and restricted problem, then we can show that <span class="math inline">\(\mathcal{E}^{S} \subseteq \mathcal{E}^{conv(S)}\)</span>.</p>
<p>Indeed, suppose there exists <span class="math inline">\(\mathcal{E}^{0}\subset \mathcal{E}^{S}\)</span> with <span class="math inline">\(vol(\mathcal{E}^{0}) &gt;0\)</span>, but <span class="math inline">\(\mathcal{E}^{0} \cap \mathcal{E}^{conv(S)} = \emptyset\)</span>. It follows that <span class="math inline">\(||A^{conv(S)}(s-c^{conv(S))||&gt;1, \forall s \in \mathcal{E}^{0}\)</span>, which implies that <span class="math inline">\(\mathcal{E}^{0} \cap conv(S) = \emptyset\)</span> and <span class="math inline">\(\mathcal{E}^{0} \cap S = \emptyset\)</span>. Since points in <span class="math inline">\(\mathcal{E}^{0}\)</span> are not in <span class="math inline">\(S\)</span>, we volume of the ellipsoid found by removing <span class="math inline">\(\mathcal{E}^{0}\)</span></p>
</div>
<div id="computational-comments" class="section level3">
<h3>Computational comments</h3>
<p>Using the formulation:</p>
<p><span class="math display">\[A^{*},c^* \in \displaystyle \arg\min_{A \in \mathbb{S}_{++}^n, c} \{ -\log(\det(A)) : ||A(p-c)||_2\leq 1 , \forall p \in P \}\]</span></p>
<p>the number of convex second order cone inequalities corresponds to the cardinality of the set <span class="math inline">\(P\)</span> (which need not be convex).</p>
<p>In the simplest case, we consider Given a reasonable <span class="math inline">\(n\)</span> and <span class="math inline">\(N\)</span>, the problem should be amenable to most off-the-self interior-point solvers. For the case study presented next, this is the approach used. It is also likely the use case for most practical applications involving polygons or other geometric objects which are represented by a finite collection of two dimensional points (e.g. longtitude and latitude). For our practical experiments, computing the minimum volume covering ellipsoid took a few seconds for polygons with 64 to 2865 points using CVXR for the modelling and using ECOS solver.</p>
</div>
</div>
</div>
<div id="case-study-evaluating-the-shape-of-cities-and-neighbourhoods" class="section level1">
<h1>Case study: evaluating the shape of cities and neighbourhoods</h1>
<p>Using this simple methodology, we can answer questions such as: is a region more ellongated than another and to what extent is a region stretched. Since <span class="math inline">\(\lambda_{max}(B)/\lambda_{min}(B) \in [1, \infty)\)</span>, any circle has minimal eigen ratio while an inifintely thin ellipsoid has an infinitely large ratio.</p>
<div id="city-wide-comparisons" class="section level2">
<h2>City-wide comparisons</h2>
<p>We begin by comparing the cities of Quebec and Montreal. As mentionned in this other <a href="https://www.charlesgauvin.ca/post/a-graph-theoretic-comparison-of-quebec-and-montreal/">post</a>, various metrics suggest that Quebec is much more circular/symmetric than Montreal. Comparing the ratio of eigenvalues for the minimum volume bounding ellipsoid of both cities confirms that this is true, Quebec has a ratio of 1.05, rather close to the lower bound of 1, while for Montreal this figure jumps up to 1.66.</p>
<p><a href="%22/images/2019/11/ellipsoid/neighbourhoodsCoveringEllipsoidsQcMtl_EntireCities.png%22">Comparing the shape of Montreal (left) and Quebec City (right)</a></p>
</div>
<div id="neighbourhood-level-comparisons" class="section level2">
<h2>Neighbourhood-level comparisons</h2>
<p>Performing the analysis for each of the 32 and 35 neighbourhoods making up Montreal and Quebec City allows us to see that Montreal is also slightly more ellongated at the neighbourhood level on average.</p>
<p><a href="%22/images/2019/11/ellipsoid/densityPlotsEigenRatiosNeigh.png%22">Comparing the eigen ratio distribution by neighbourhood</a></p>
<p>This is mostly due to the fact that Montreal has some large ellongated outliers. Saint-Henri and Rivière des prairies both have a height more than 3.5 times its width.</p>
<p><a href="%22/images/2019/11/ellipsoid/lollipopEigenRatioMontreal.png%22">Comparing the eigen ratio distribution by neighbourhood</a></p>
<p>Although Quebec is less variable at the neighbouhood level, it also has some relatively ellongated neighbourhoods. Nonetheless, we also observe that 8 neighbourhoods in Quebec have a ratio no larger than the ratio of LaSalle, the minimal neighbourhood in Montreal.</p>
<p><a href="%22/images/2019/11/ellipsoid/lollipopEigenRatioQuebec.png%22">Comparing the eigen ratio distribution by neighbourhood</a></p>
<p>All 3 most ellongated neighbourhoods in Montreal are adjacent to a water body while for Quebec City this is true for 2 of the 3 most ellongated neighbourhoods. In the case of Montreal, Saint-Henri is delimited by the canal Lachine at the south-east while Sillery is delimited by the Saint-Lawrence River.</p>
<p><a href="%22/images/2019/11/ellipsoid/neighbourhoodsCoveringEllipsoidsMontreal_Ellongated.png%22">Most ellongated neighbourhoods for Montreal: Saint-Henri (left) and Quebec City - Sillery (right)</a></p>
<p>The most neighbourhoods with the smallest eigen ratios have remarkably symmetric distribution. Quartier 4-1 in Beauport, Quebec is almost a perfect square. It seems probable that various neighbourhoods without any clear natural or sociological delimitations could have more symmetric shapes. This might namely be the case for more recently developped suburbs with few natural constraints and that are at least partly delimited by highways. For Quebec City at least, this seems to be somewhat corroborated. However, this is an obvious overgeneralization as various factors can influence the shape of neighbourhoods.</p>
<p><a href="%22/images/2019/11/ellipsoid/neighbourhoodsCoveringEllipsoidsQcMtl_Symmetric.png%22">Most symmetric neighbourhoods for Montreal: LaSalle (left) and Quebec City - Quartier 4-1 (right)</a></p>
<p>Overlaying all neighbourhoods and covering ellipsoids also makes for some interesting art:</p>
<p><a href="%22/images/2019/11/ellipsoid/neighbourhoodsCoveringEllipsoidsQuebec_AllNeighbourhoods.png%22">Quebec city neighbourhoods and minimum volume covering ellipsoids</a></p>
<p><a href="%22/images/2019/11/ellipsoid/neighbourhoodsCoveringEllipsoidsMontreal_AllNeighbourhoods.png%22">Montreal neighbourhoods and minimum volume covering ellipsoids</a></p>
</div>
</div>
