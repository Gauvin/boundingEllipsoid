
getEigenValueRatio <- function(B){
  
  
  ratio <- eigen(B)$values[[1]]/eigen(B)$values[[2]]
  
  stopifnot(ratio>=1 )
  
  return( ratio )
}


getEigenValueRatioAllNeigh <- function(shp, idStr, city){
  
  listEllipsoids <- getEllipsoidAllNeigh(shp,idStr, city)
  listEigenValueRatios <- map( 1:length(listEllipsoids), ~ getEigenValueRatio(listEllipsoids[[.x]]$B) ) %>% 
    set_names(names(listEllipsoids))
  
}

