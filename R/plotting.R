

plotEntireCityWithEllipsoid <- function(ellResults, shp, city, captionStr="Entire city", extraStr="EntireCity"){
  
  dfEllipsoid <- getEllipsoidPointsOrigScale(ellResults, shp, 1 )
  
  dfEllipsoidLine <- dfEllipsoid[, c("V1Raw","V2Raw")]
  dfEllipsoidLine %<>% rbind( dfEllipsoid[ 1, c("V1Raw","V2Raw")] ) 
  
  ###Cast sf object with polygon geometry + set crs
  shpPolyEllipsoid <- sf::st_polygon(list(dfEllipsoidLine %>% as.matrix)) %>% 
    st_sfc() %>% 
    st_sf(crs=st_crs(shp))
  
  #Tweak for city name string
  city <- ifelse(city =="Qc","Quebec",city)
  city <- ifelse(city =="Mtl","Montreal",city)
  
  p <- ggplot() + 
    geom_sf(data=shp ) + 
    geom_sf(data=shpPolyEllipsoid , size=1 , shape=3 , alpha=0.5) +
    ggtitle(glue("Minimum volume covering ellipsoid - {city} ")) + 
    labs(caption = glue("{captionStr}"))  + 
    theme(legend.position = "none")
  
  
  ggsave( filename = here("Figures",glue("neighbourhoodsCoveringEllipsoids{city}_{extraStr}.png")),
          plot=p )
  
  (p)
}



plotAllNeighWithEllipsoid <- function(listEll, shp, idStr,city, extraStr="", captionStr="", addTitleAndCaption=T){

  
  #Quick check to make sure number of ellipsoids and number of features correspond
  if(nrow(shp) != length(listEll)){
    print(glue("Warning there are {nrow(shp)} features, but {length(listEll)} ellipsoids"))
  }
  
  #We need at least to have a shape for every ellipsoid
  stopifnot(nrow(shp) >= length(listEll))
  
  #Plot the first neighbourhood
  ##Get the ellipsoid polygon in the original scale
  shpPolyEllipsoid <- getEllipsoidPolygonOrigScale (listEll[[1]], shp, idStr, 1)
  
  ###Update the neighbourhood
  shpPolyEllipsoid$neigh <- listEll[1] %>% names
  
  listNamesNeighSuccesss <- list(shpPolyEllipsoid$neigh)
  #Plot the rest of the neighbourhoods
  if(length(listEll)>=2){
  for(k in 2:length(listEll)){
    
    tryCatch({
      shpPolyEllipsoidNew <- getEllipsoidPolygonOrigScale (listEll[[k]], shp, idStr, k)
      
      ###Update the neighbourhood
      shpPolyEllipsoidNew$neigh <- listEll[k] %>% names
      
      shpPolyEllipsoid <-  rbind( shpPolyEllipsoid, shpPolyEllipsoidNew )
      
      listNamesNeighSuccesss <- rlist::list.append(listNamesNeighSuccesss, shpPolyEllipsoidNew$neigh)
    }, error=function(e){
      strErr <- conditionMessage(e)
      print(glue("Fatal error at index {k}: {strErr}"))
    })
  
  }
  }

  
  #Add a newline every 5 neighbourhoods (for Qc city with 35 neigh and mtl with 32 this yields 7 lines)
  if(length(listNamesNeighSuccesss)>=6){
    idxSkip <- seq(6,length(listNamesNeighSuccesss), 5)
    boolSkip <- 1:length(listNamesNeighSuccesss) %in% idxSkip
    boolNoSkip <- !(1:length(listNamesNeighSuccesss) %in% idxSkip)
    listNamesNeighSuccesss[boolSkip] <- paste( listNamesNeighSuccesss[boolSkip], "\n")
    listNamesNeighSuccesss[boolNoSkip] <- paste( listNamesNeighSuccesss[boolNoSkip], ",")
  } 

  
  #Paste them all together (only if not all were successfull)
  if(captionStr == ""){
    allNeighNames <- paste(listNamesNeighSuccesss, sep=" ",collapse=" ")
  }else{
    allNeighNames <- captionStr
  }

  
  #Make sure we only select a subset of neighbourhoods
  shpSubset <- shp[shp[[idStr]] %in% shpPolyEllipsoid$neigh, ]
  
  if(nrow(shpSubset) != n_distinct(shpPolyEllipsoid$neigh) ){
    print(glue("Watch out! should have {n_distinct(shpPolyEllipsoid$neigh)} neigh, but only {nrow(shpSubset)} rows to plot"))
  }
  
  
  #Tweak for city name string
  city <- ifelse(city =="Qc","Quebec",city)
  city <- ifelse(city =="Mtl","Montreal",city)
  
  p <- ggplot() + 
    geom_sf(data=shpSubset, aes_string( col=idStr)) + 
    geom_sf(data=shpPolyEllipsoid, aes( col=neigh) , size=1 , shape=3 , alpha=0.5) +
    theme(legend.position = "none")
  
  #Add title if desired
  if(addTitleAndCaption){
    p <- p   +
      ggtitle(glue("Neighbourhoods and minimum volume covering ellipsoids - {city} ")) + 
      labs(caption = glue("Neighbourhoods:\n{allNeighNames}"))  
      ggsave( filename = here("Figures",glue("neighbourhoodsCoveringEllipsoids{city}_{extraStr}.png")),
              plot=p )
  }else{
    ggsave( filename = here("Figures",glue("neighbourhoodsCoveringEllipsoids{city}_{extraStr}NoCaptions.png")),
            plot=p )
  }
  
  

  
  (p)
  
  return(p)
}



plotLollipop <- function(dfEigen, city){
  
  stopifnot("neigh" %in% colnames(dfEigen) & "eigenRatio" %in% colnames(dfEigen))
  
  dfEigen %<>% arrange(desc(eigenRatio))
  dfEigen %<>% mutate(neigh = factor(neigh, levels = as.character(unique(dfEigen$neigh))))
  dfEigen$avgEigRatio <- mean(dfEigen$eigenRatio)
  
  #Tweak for city name string
  city <- ifelse(city =="Qc","Quebec",city)
  city <- ifelse(city =="Mtl","Montreal",city)
  
  pBar <- ggplot(dfEigen) + 
    geom_point(aes(x=neigh,y=avgEigRatio)) + 
    geom_segment(aes(x=neigh,xend=neigh, y=avgEigRatio,yend=eigenRatio)) + 
    geom_text(aes(x=neigh,y=eigenRatio,label=round(eigenRatio,2)), color="black", size=4) +
    coord_flip() +
    xlab("Neighbourhood") + 
    ylab("Eigen ratio") + 
    labs(caption=glue("Average neighbourhood eigen ratio: {round( mean(dfEigen$eigenRatio),2)}")) +
    ggtitle(glue("Neighbourhood shapes - {city}"))
  
  (pBar)
  
  ggsave(here("Figures",glue("lollipopEigenRatio{city}.png")),
         pBar)
  
  return(pBar)
}
